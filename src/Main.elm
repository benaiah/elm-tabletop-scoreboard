-- This file is part of Elm Tabletop Scoreboard
-- Copyright (C) 2018  Aidan Gauland

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

import Browser
import Html exposing (Html, button, div, text)
import Html.Events exposing (onClick)
import Array exposing (Array)
import Player exposing (Player)

-- MAIN

main =
  Browser.sandbox { init = init, update = update, view = view }


-- MODEL

type alias Model = Array Player

init : Model
init =
  Array.repeat 2 Player.zeroed


-- UPDATE

type Msg
  = Increment Int
  | Decrement Int
  | AddPlayer
  | RemovePlayer Int


updateArray : Int -> (a -> a) -> Array a -> Array a
updateArray index updateFunc array =
  case Array.get index array of
    Nothing ->
      array

    Just value ->
      Array.set index (updateFunc value) array


removeFromArray : Int -> Array a -> Array a
removeFromArray index array =
  Array.append
    (Array.slice 0 index array)
    (Array.slice (index + 1) (Array.length array) array)


update : Msg -> Model -> Model
update msg model =
  case msg of
    Increment index ->
      updateArray index (\x -> { x | money = x.money + 1 }) model

    Decrement index ->
      updateArray index (\x -> { x | money = x.money - 1 }) model

    AddPlayer ->
      Array.push Player.zeroed model

    RemovePlayer index ->
      removeFromArray index model

-- VIEW

view : Model -> Html Msg
view model =
  div []
    ((Array.toList (Array.indexedMap viewPlayer model)) ++
     [ button [ onClick AddPlayer ] [ text "++" ]
     ])

viewPlayer : Int -> Player -> Html Msg
viewPlayer index player =
  div []
    [ text ("Player " ++ String.fromInt index)
    , button [ onClick (RemovePlayer index) ] [ text "--" ]
    , viewPlayerMoney index player
    , viewPlayerScore player
    ]

viewPlayerMoney : Int -> Player -> Html Msg
viewPlayerMoney index player =
  div []
    [ button [ onClick (Decrement index) ] [ text "-" ]
    , div [] [ text ("Money: " ++ (String.fromInt player.money)) ]
    , button [ onClick (Increment index) ] [ text "+" ]
    ]

viewPlayerScore : Player -> Html Msg
viewPlayerScore player =
  div []
    [ text ("Score: " ++ (String.fromInt (Player.score player)))
    ]
